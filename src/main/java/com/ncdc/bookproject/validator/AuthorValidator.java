package com.ncdc.bookproject.validator;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AuthorValidator implements ConstraintValidator<Author, String> {
    @Override
    public void initialize(Author author) {

    }
    public boolean spliting(String s) {
        boolean flag = false;
        String[] splitStr = s.split("\\s+");
        for (int i = 0; i < splitStr.length; i++) {
            if (splitStr[i].charAt(0) == 'A') {
                flag = true;
                return flag;
            } else flag = false;
        }
        return flag;

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {

        return spliting(s);
    }

}
