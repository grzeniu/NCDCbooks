package com.ncdc.bookproject.validator;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = AuthorValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Author {


    String message() default "Wrong author name. Author must have A letter at begin of Name or Suranme";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
