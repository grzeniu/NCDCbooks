package com.ncdc.bookproject.data;

import com.ncdc.bookproject.model.Book;

import java.util.ArrayList;
import java.util.List;

public class BookData {

	private static List<Book> books;

	public static List<Book> getBooks() {
		if (books == null) {
			books = new ArrayList<>();
		}
		return books;
	}

	private BookData() {
	}
}
