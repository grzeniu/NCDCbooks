package com.ncdc.bookproject.controllers;

import com.ncdc.bookproject.data.BookData;
import com.ncdc.bookproject.model.Book;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;


@Controller
public class BookController {
    private static final Logger logger= Logger.getLogger(BookController.class);

    @GetMapping("/view")
    public ModelAndView viewBooks() {
        ModelAndView model = new ModelAndView("viewBooks");
        model.addObject("booksList", BookData.getBooks());
        return model;
    }

    @GetMapping(value = "/add")
    public String addPage(Model model){
        logger.info("Return book add page");
        model.addAttribute("book",new Book());
        return "add";
    }

    @PostMapping(value = "/add")
    public String addBookPage(@Valid Book book, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            logger.info("Getting back to form page");
            return "add";
        }
        logger.info("Return succesAdding page");
        model.addAttribute("book",book);
        BookData.getBooks().add(book);
        return "redirect:view";
    }


}
