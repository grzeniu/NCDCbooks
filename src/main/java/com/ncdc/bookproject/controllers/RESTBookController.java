package com.ncdc.bookproject.controllers;

import com.ncdc.bookproject.data.BookData;
import com.ncdc.bookproject.model.Book;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/rest")
public class RESTBookController {
	private static final org.apache.log4j.Logger logger= org.apache.log4j.Logger.getLogger(BookController.class);

	@PostMapping(value = "/addBook")
	public @ResponseBody Book addBook(@RequestBody Book book) {
		BookData.getBooks().add(book);
		logger.info("Adding book: " + book);
		return book;
	}

	@GetMapping(value = "/getAllBooks")
	public List<Book> getAllBooks(){
		logger.info("Returning book list");
		return BookData.getBooks();
	}

}

