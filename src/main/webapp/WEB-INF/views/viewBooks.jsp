<%--
  Created by IntelliJ IDEA.
  User: Grzes
  Date: 08.12.2017
  Time: 10:20
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <title>NCDC library page</title>
</head>
<body>
<center>
    <div id="container">
       <h1><b>Welcome to NCDC library</b></h1>
        <br/>
        <h2><a href="view">Refresh</a></h2>
        </p>
       <h2><b>Bestseller List:</b></h2>
        <div>
            <table>
            <thead>
            <tr>
                <th>Title</th>
                <th>Author Name</th>
                <th>ISBN</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="listValue" items="${booksList}">
            <tr>
                <td>${listValue.title}</td>
                <td>${listValue.author}</td>
                <td>${listValue.isbn}</td>
            </tr>
            </c:forEach>
            </tbody>
            </table>

        </div>
        <c:if test="${empty booksList}">
            <i><p>The list is empty.</p></i>
        </c:if>
        <a href="add">Add new record to list</a>
        <br/>
    </div>
</center>
</body>
</html>